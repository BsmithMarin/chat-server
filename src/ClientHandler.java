
import java.io.*;
import java.net.Socket;


/**
 * @author Brahyan Marin
 * Clase que gestiona la comunicacion con el cliente (socket) pasado en el constructor.
 * En funcion del tipo de mensaje que recibe notifica a los demás clientes conectados
 */

public class ClientHandler extends Thread{

    public Socket cliente;

    public BufferedWriter writer;
    public BufferedReader reader;
    private String nickname="";

    public ClientHandler(Socket socketCliente){
        try {
            cliente = socketCliente;
            writer = new BufferedWriter( new OutputStreamWriter( cliente.getOutputStream() ));
            reader = new BufferedReader( new InputStreamReader( cliente.getInputStream() ));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void run(){

        try {

            String paqueteRecibido;
            while ( (paqueteRecibido = reader.readLine() )!= null ){

                String[] argumentos = paqueteRecibido.split(";");
                switch ( argumentos[0] ){

                    case "NUEVA_CONEXION":

                        nickname = argumentos[1];
                        nuevaConexion();
                        ConexionesActivas.notifyNewConnection(nickname);
                        enviarUsuariosConectados();
                        break;

                    case "MENSAJE":
                        enviarMensaje(argumentos[1],argumentos[2]);
                        break;
                }
            }

        } catch (IOException  e) {
            System.err.println("Error");
        }finally {
            System.err.printf("FINALIZADA CONEXION CON CLIENTE CON ip: %s \n", cliente.getInetAddress().getHostAddress());
            if(nickname != null)
                ConexionesActivas.deleteConnection(nickname);
            if(!cliente.isClosed()){
                try {
                    cliente.close();
                } catch (IOException e) {
                    System.err.println("*****Error al cerrar comunicacion con el cliente "+
                            cliente.getInetAddress().getHostAddress());
                }
            }
        }
    }

    private void enviarUsuariosConectados() throws IOException {
        String[] usuariosConectados= {"LISTA_CONECTADOS"} ;
        ConexionesActivas.getConexionesActivas().forEach((key,value)->{
            if(!key.equals(nickname))
                usuariosConectados[0] += (";"+key);
        });

        writeLine(usuariosConectados[0]);
    }

    /**
     * Regista en el mapa de conexiones activas al cliente actual
     */
    private void nuevaConexion() {

        ConexionesActivas.addConnection(nickname,this);
        System.out.printf("Añadio cliente %s con IP: %s \n",nickname,cliente.getInetAddress().getHostAddress());
        System.out.printf("Clientes conectados: %s \n",ConexionesActivas.getConexionesActivas());
    }

    private void enviarMensaje(String destinatario,String mensaje){

        try{
            if(!destinatario.equals("GLOBAL")){
                String paqueteMensaje = "MENSAJE;"+nickname+";"+mensaje;
                ClientHandler clientHandler = ConexionesActivas.getConexionesActivas().get(destinatario);
                clientHandler.writeLine(paqueteMensaje);
            }else{
                String paqueteMensaje = "MENSAJE;GLOBAL;"+nickname+": "+mensaje;
                ConexionesActivas.getConexionesActivas().forEach((key,value)->{
                    if(!key.equals(nickname)){
                        try{
                            value.writeLine(paqueteMensaje);
                        }catch (IOException e){
                            System.err.println("ERROR AL MANDAR MENSAJE GLOBAL");
                            e.printStackTrace();
                        }
                    }
                });
            }

        }catch (IOException e){
            System.err.printf("ERROR AL MANDAR EL MENSAJE DE %s a %s \n",nickname,destinatario);
            e.printStackTrace();
        }

    }

    /**
     * Escribe linea al cliente usando el stream de salida
     * @param linea
     * @throws IOException
     */
    public synchronized void writeLine(String linea) throws IOException{
            writer.write(linea);
            writer.newLine();
            writer.flush();
    }
}
