import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

public class ServidorChat {


    /**
     * @author Brahyan Marin
     * Crea el servidor del chat, mediante un ServerSocket acepta solicitudes de los diferentes clientes
     * y se las pasa a un manejador de clientes para liberear este hilo para atender más solicitudes.
     * @param args
     */
    public static void main(String[] args) {

        try {
            Thread.currentThread().setPriority(Thread.MAX_PRIORITY);
            ServerSocket socketServidor = new ServerSocket(9000);
            System.out.print("---ESPERANDO CONEXIONES EN EL PUERTO 9000 -----------\n");
            while (true){
                Socket cliente = socketServidor.accept();
                System.out.printf("Nueva conexion desde %s:%n\n",cliente.getInetAddress().getHostAddress(),cliente.getPort());
                ClientHandler hiloCliente = new ClientHandler(cliente);
                hiloCliente.start();

            }

        } catch (IOException e) {
            e.printStackTrace();
        }

    }

}
