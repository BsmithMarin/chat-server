
import java.util.concurrent.ConcurrentHashMap;

public class ConexionesActivas {

    private static final ConcurrentHashMap<String, ClientHandler> conexionesActivas = new ConcurrentHashMap<>();

    private ConexionesActivas(){

    }

    public static synchronized void addConnection(String nickname, ClientHandler clientHandler){

        conexionesActivas.put(nickname,clientHandler);
    }

    public static synchronized void deleteConnection(String nickname){

        conexionesActivas.remove(nickname);
    }


    public static synchronized void notifyNewConnection(String nickname){

        conexionesActivas.forEach((key,value)->{
            try{
                if(!nickname.equals(key)){
                    value.writeLine("NUEVA_CONEXION;"+nickname);
                }

            }catch (Exception e){
                System.err.printf("Error al notificar la conexion del cliente %s al cliente %s \n",nickname,key);
            }
        });
    }

    public static ConcurrentHashMap<String,ClientHandler> getConexionesActivas(){
        return conexionesActivas;
    }

}
